﻿using System.Collections.Specialized;
using Sitecore;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Links;
using Vhs.SiteSpecificLinkProvider.Extensions;

namespace Vhs.SiteSpecificLinkProvider.Providers
{
    public class VhsLinkProvider : LinkProvider
    {
        public bool EnsurePostfixSlash { get; set; }

        public override void Initialize(string name, NameValueCollection config)
        {
            base.Initialize(name, config);

            EnsurePostfixSlash = MainUtil.GetBool(config["ensurePostfixSlash"], false);
        }

        public override string GetItemUrl(Item item, UrlOptions options)
        {
            Assert.ArgumentNotNull(item, nameof(item));
            Assert.ArgumentNotNull(options, nameof(options));

            if (EnsurePostfixSlash)
            {
                return base.GetItemUrl(item, options).EnsurePostfixSlashForLink();
            }

            return base.GetItemUrl(item, options);
        }
    }
}