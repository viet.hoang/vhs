﻿namespace Vhs.SiteSpecificLinkProvider.Extensions
{
    public static class StringExtensions
    {
        #region HasText
        public static bool HasText(this string source)
        {
            return !string.IsNullOrWhiteSpace(source);
        }
        #endregion

        #region EnsurePostfixSlashForLink
        public static string EnsurePostfixSlashForLink(this string link)
        {
            if (link.Contains("#") || link.Contains("?"))
            {
                return link;
            }

            return Sitecore.StringUtil.EnsurePostfix('/', link);
        }
        #endregion
    }
}