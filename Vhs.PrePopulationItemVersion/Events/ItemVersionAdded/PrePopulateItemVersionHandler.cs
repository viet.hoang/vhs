﻿using System;
using System.Linq;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Events;
using Sitecore.Globalization;
using Sitecore.StringExtensions;
using Vhs.PrePopulationItemVersion.Services;

namespace Vhs.PrePopulationItemVersion.Events.ItemVersionAdded
{
    public class PrePopulateItemVersionHandler
    {
        public void PrePopulateItemVersion(object sender, EventArgs args)
        {
            var currentItem = Event.ExtractParameter(args, 0) as Item;
            if (currentItem == null)
            {
                return;
            }

            var itemSiteInfo =
                Sitecore.Configuration.Factory.GetSiteInfoList()?
                    .FirstOrDefault(
                        x =>
                            !x.RootPath.IsNullOrEmpty() && !x.StartItem.IsNullOrEmpty() &&
                            !x.RootPath.Equals(Sitecore.Constants.ContentPath, StringComparison.OrdinalIgnoreCase) &&
                            currentItem.Paths.FullPath.StartsWith(x.RootPath, StringComparison.OrdinalIgnoreCase));

            if (itemSiteInfo == null)
            {
                return;
            }

            const string pipeSeparator = "|";
            const string colonSeparator = ":";
            var siteList = ConfigurationService.SiteList.Split(new[] { pipeSeparator },
                StringSplitOptions.RemoveEmptyEntries);

            var isAbleToPrePopulate = false;
            var defaultSourceLanguage = ConfigurationService.DefaultSourceLanguage;
            foreach (var site in siteList)
            {
                var segments = site.Split(new[] { colonSeparator }, StringSplitOptions.RemoveEmptyEntries);
                var siteName = segments[0];
                if (itemSiteInfo.Name.Equals(siteName, StringComparison.OrdinalIgnoreCase))
                {
                    isAbleToPrePopulate = true;
                    defaultSourceLanguage = segments.Length > 1 ? segments[1] : defaultSourceLanguage;
                    break;
                }
            }

            if (!isAbleToPrePopulate)
            {
                return;
            }

            Language sourceLanguage;
            Language.TryParse(defaultSourceLanguage, out sourceLanguage);
            if (sourceLanguage == null)
            {
                return;
            }

            var currentDatabase = Sitecore.Context.ContentDatabase;
            if (currentDatabase == null)
            {
                return;
            }

            var prototypeItem = currentDatabase.GetItem(currentItem.ID, sourceLanguage).Versions.GetLatestVersion();
            if (prototypeItem == null)
            {
                Log.Error(
                    string.Format("Error in {0}: {1} version of {2} is NOT existent.",
                        typeof (PrePopulateItemVersionHandler).FullName, itemSiteInfo.Language,
                        currentItem.Paths.FullPath), this);
                return;
            }

            prototypeItem.Fields.ReadAll();
            using (new EditContext(currentItem))
            {
                foreach (Sitecore.Data.Fields.Field field in prototypeItem.Fields)
                {
                    if (!field.Shared && !field.Name.StartsWith("__") && !field.Value.IsNullOrEmpty())
                    {
                        currentItem.Fields[field.ID].SetValue(field.Value, force: true);
                    }
                }
            }
        }
    }
}