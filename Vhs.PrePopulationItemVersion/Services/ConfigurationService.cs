﻿using SC = Sitecore;

namespace Vhs.PrePopulationItemVersion.Services
{
    public static class ConfigurationService
    {
        public static string SiteList
            =>
                SC.Configuration.Settings.GetSetting(
                    "Vhs.PrePopulationItemVersion.SiteList",
                    string.Empty);

        public static string DefaultSourceLanguage
            =>
                SC.Configuration.Settings.GetSetting("Vhs.PrePopulationItemVersion.DefaultSourceLanguage",
                    "en");
    }
}