﻿using System;
using SC = Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Resources.Media;
using Sitecore.Web;
using Sitecore.Web.UI.Pages;
using Vhs.MediaPublicUrlPickerField.Constants;
using Vhs.MediaPublicUrlPickerField.Services;

namespace Vhs.MediaPublicUrlPickerField.Dialogs
{
    public class MediaPublicUrlPickerDialog : DialogForm
    {
        private const string Separator = "|";

        private readonly Database _masterDb = Factory.GetDatabase("master");

        public SC.Web.UI.HtmlControls.Image ImageFrame;

        public SC.Web.UI.HtmlControls.Edit TextBoxPublicUrl;

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (TextBoxPublicUrl == null) return;

            var containerId = WebUtil.GetQueryString(QueryStringKeys.ContainerId);

            var currentItem = _masterDb.Items.GetItem(containerId);

            if (currentItem == null) return;

            var medialUrl = MediaManager.GetMediaUrl(_masterDb.Items.GetItem(currentItem.ID));

            var itemHasDomain = currentItem.Parent;

            var domainCandidate = string.Empty;
            while (itemHasDomain != null)
            {
                var domainFieldNames = ConfigurationService.DomainFieldName.Split(new[] { Separator },
                StringSplitOptions.RemoveEmptyEntries);
                foreach (var domainFieldName in domainFieldNames)
                {
                    domainCandidate = itemHasDomain.Fields[domainFieldName] != null
                        ? itemHasDomain.Fields[domainFieldName].Value
                        : string.Empty;
                    if (!string.IsNullOrWhiteSpace(domainCandidate)) break;
                }

                if (string.IsNullOrWhiteSpace(domainCandidate)) itemHasDomain = itemHasDomain.Parent;
                else break;
            }

            var publicDomain = !string.IsNullOrWhiteSpace(domainCandidate)
                ? domainCandidate
                : ConfigurationService.DefaultPublicDomain;

            TextBoxPublicUrl.Value = medialUrl.Replace("/sitecore/shell/",
                SC.StringUtil.EnsurePostfix('/', publicDomain));
        }
    }
}