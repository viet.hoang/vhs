﻿using SC = Sitecore;

namespace Vhs.MediaPublicUrlPickerField.Services
{
    public static class ConfigurationService
    {
        public static string DialogWidth
            => SC.Configuration.Settings.GetSetting("Vhs.MediaPublicUrlPickerField.DialogWidth", "800");

        public static string DialogHeight
            => SC.Configuration.Settings.GetSetting("Vhs.MediaPublicUrlPickerField.DialogHeight", "150");

        public static string DomainFieldName
            =>
                SC.Configuration.Settings.GetSetting("Vhs.MediaPublicUrlPickerField.DomainFieldName",
                    string.Empty);

        public static string DefaultPublicDomain
            =>
                SC.Configuration.Settings.GetSetting("Vhs.MediaPublicUrlPickerField.DefaultPublicDomain",
                    string.Empty);
    }
}