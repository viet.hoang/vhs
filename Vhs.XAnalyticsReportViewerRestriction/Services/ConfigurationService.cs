﻿using SC = Sitecore;

namespace Vhs.XAnalyticsReportViewerRestriction.Services
{
    public static class ConfigurationService
    {
        public static bool Enabled
        {
            get
            {
                var xmlAttributes = SC.Configuration.Factory.GetConfigNode("vhsXAnalyticsReportViewerRestriction")
                    ?.Attributes;
                if (xmlAttributes == null) return false;
                bool enabled;
                return bool.TryParse(xmlAttributes["enabled"]?.Value, out enabled);
            }
        }

        public static string SuperReportViewerRoles => SC.Configuration.Factory.GetConfigNode("vhsXAnalyticsReportViewerRestriction/superReportViewerRoles")
            ?.InnerXml;

        public static string SitesAndReportViewerRoleList => SC.Configuration.Factory.GetConfigNode("vhsXAnalyticsReportViewerRestriction/sitesAndReportViewerRoles")
            ?.InnerXml;
    }
}