﻿using SC = Sitecore;

namespace Vhs.SyncMediaFolderProcessor.Services
{
    public static class ConfigurationService
    {
        public static string SitecoreMediaFolder
            => SC.Configuration.Settings.GetSetting("Vhs.SyncMediaFolderProcessor.SitecoreMediaFolder", string.Empty);

        public static string SyncMediaFolder
            => SC.Configuration.Settings.GetSetting("Vhs.SyncMediaFolderProcessor.TargetSyncMediaFolder", string.Empty);
    }
}