﻿using System;
using System.IO;
using Microsoft.Synchronization;
using Microsoft.Synchronization.Files;
using Sitecore.Publishing.Pipelines.Publish;
using Sitecore.Diagnostics;
using Vhs.SyncMediaFolderProcessor.Services;

namespace Vhs.SyncMediaFolderProcessor.Pipelines.Publish
{
    public class SyncMediaFolderProcessor
    {
        public void Process(PublishContext context)
        {
            var srcPath = ConfigurationService.SitecoreMediaFolder;
            var destPath = ConfigurationService.SyncMediaFolder;
            this.SyncMediaFolder(srcPath, destPath, new FileSyncScopeFilter(), FileSyncOptions.None);
        }

        public void SyncMediaFolder(string sourceReplicaRootPath, string destinationReplicaRootPath,
            FileSyncScopeFilter filter, FileSyncOptions options)
        {
            try
            {
                this.PrepareDirectory(sourceReplicaRootPath);
                this.PrepareDirectory(destinationReplicaRootPath);
            }
            catch (Exception ex)
            {
                Log.Error($"SyncMediaFileProcessor - SyncMediaFolder: {ex.Message}", this);
                Log.Error($"SyncMediaFileProcessor - SyncMediaFolder: {ex.StackTrace}", this);
                return;
            }

            FileSyncProvider sourceProvider = null;
            FileSyncProvider destinationProvider = null;
            try
            {
                sourceProvider = new FileSyncProvider(sourceReplicaRootPath, filter, options);
                destinationProvider = new FileSyncProvider(destinationReplicaRootPath, filter, options);
                var agent = new SyncOrchestrator
                {
                    LocalProvider = sourceProvider,
                    RemoteProvider = destinationProvider,
                    Direction = SyncDirectionOrder.Upload
                };

                Log.Info($"Synchronizing the media folder changes to: {destinationProvider.RootDirectoryPath}", this);
                agent.Synchronize();
            }
            finally
            {
                sourceProvider?.Dispose();
                destinationProvider?.Dispose();
            }
        }

        public void PrepareDirectory(string directoryName)
        {
            if (!Directory.Exists(directoryName))
            {
                Directory.CreateDirectory(directoryName);
            }
        }
    }
}