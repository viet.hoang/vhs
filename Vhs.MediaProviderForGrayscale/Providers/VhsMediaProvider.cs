﻿using System;
using System.Linq;
using Sitecore.Abstractions;
using Sitecore.Data.Items;
using Sitecore.DependencyInjection;
using Sitecore.Resources.Media;
using Vhs.MediaProviderForGrayscale.Services;

namespace Vhs.MediaProviderForGrayscale.Providers
{
    public class VhsMediaProvider : MediaProvider
    {
        public VhsMediaProvider() : base((BaseFactory)ServiceLocator.ServiceProvider.GetService(typeof(BaseFactory)))
        {
        }

        public VhsMediaProvider(BaseFactory factory) : base(factory)
        {
        }

        public override string GetMediaUrl(MediaItem item, MediaUrlOptions options)
        {
            var url = base.GetMediaUrl(item, options);
            if (IsGrayscaleable(item.Extension))
            {
                url = $"{url}{(url.Contains("?") ? "&" : "?")}gray=1";
            }

            return url;
        }

        private static bool IsGrayscaleable(string extension)
        {
            if (!ConfigurationService.Enabled) return false;

            var extensions = ConfigurationService.SupportedExtensions;
            var supportedExtensions = extensions.Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
            return supportedExtensions.Contains(extension);
        }
    }
}
