﻿using SC = Sitecore;

namespace Vhs.MediaProviderForGrayscale.Services
{
    public static class ConfigurationService
    {
        public static bool Enabled
            =>
                SC.MainUtil.GetBool(SC.Configuration.Settings.GetSetting("Vhs.MediaProviderForGrayscale.Enabled",
                    "false"), false);

        public static string SupportedExtensions
            => SC.Configuration.Settings.GetSetting("Vhs.MediaProviderForGrayscale.SupportedExtensions", "jpg|jpeg|jfif|png|bmp|tiff|gif|svg");
    }
}