﻿using SC = Sitecore;

namespace Vhs.SublayoutRenderingAndHybridPlaceholder.Services
{
    public static class ConfigurationService
    {
        public static string SublayoutRenderingId
            => SC.Configuration.Settings.GetSetting("Vhs.SublayoutRenderingAndHybridPlaceholder.SublayoutRenderingId", string.Empty);
    }
}