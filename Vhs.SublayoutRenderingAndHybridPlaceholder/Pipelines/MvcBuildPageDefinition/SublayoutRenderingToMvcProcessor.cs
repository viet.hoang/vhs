﻿using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Mvc.Pipelines;
using Sitecore.Mvc.Pipelines.Response.BuildPageDefinition;
using Sitecore.Mvc.Presentation;
using System.Linq;
using Vhs.SublayoutRenderingAndHybridPlaceholder.Services;

namespace Vhs.SublayoutRenderingAndHybridPlaceholder.Pipelines.MvcBuildPageDefinition
{
    public class SublayoutRenderingToMvcProcessor : BuildPageDefinitionProcessor
    {
        /// <summary>
        ///     Processes the specified BuildPageDefinitionArgs arguments.
        /// </summary>
        /// <param name="args">The BuildPageDefinitionArgs arguments.</param>
        public override void Process(BuildPageDefinitionArgs args)
        {
            foreach (var rendering in args.Result.Renderings.Where(IsSublayout))
            {
                SwitchRenderings(args, rendering);
            }
        }

        /// <summary>
        ///     Switches the renderings.
        /// </summary>
        /// <param name="args">The arguments.</param>
        /// <param name="rendering">The rendering.</param>
        private static void SwitchRenderings(MvcPipelineArgs args, Rendering rendering)
        {
            var sublayoutRenderingId = ConfigurationService.SublayoutRenderingId;
            const string VIEW_RENDERING_TYPE_NAME = "View rendering";

            var item = args.PageContext.Database.GetItem(ID.Parse(sublayoutRenderingId));
            var renderingItem = new RenderingItem(item);
            AddControlPathRenderingParameter(rendering);
            rendering.RenderingItem = renderingItem;
            rendering.RenderingType = VIEW_RENDERING_TYPE_NAME;
        }

        /// <summary>
        ///     Determines whether the specified rendering is sublayout.
        /// </summary>
        /// <param name="rendering">The rendering.</param>
        /// <returns><c>true</c> if the specified rendering is sublayout, otherwise <c>false</c>.</returns>
        private static bool IsSublayout(Rendering rendering)
        {
            return rendering.RenderingItem != null && rendering.RenderingItem.TagName == "Sublayout";
        }

        /// <summary>
        ///     Adds the control path rendering parameter.
        /// </summary>
        /// <param name="rendering">The rendering.</param>
        private static void AddControlPathRenderingParameter(Rendering rendering)
        {
            const string PARAMETERS_FIELD_NAME = "Parameters";
            const string PATH_FIELD_NAME = "Path";

            var parameterString = rendering.RenderingItem.InnerItem[PARAMETERS_FIELD_NAME];
            if (parameterString.Length > 0)
            {
                parameterString += "&";
            }

            rendering.Parameters =
                new RenderingParameters(string.Concat(parameterString, Constants.ParameterTemplate.SublayoutRenderingPath + "=",
                    rendering.RenderingItem.InnerItem[PATH_FIELD_NAME]));
        }
    }
}