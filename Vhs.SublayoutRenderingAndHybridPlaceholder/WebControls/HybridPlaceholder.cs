﻿using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Mvc.Pipelines.Response.RenderPlaceholder;
using Sitecore.Mvc.Presentation;
using Sitecore.Pipelines;
using System.IO;
using System.Web.UI;

namespace Vhs.SublayoutRenderingAndHybridPlaceholder.WebControls
{
    public class HybridPlaceholder : Sitecore.Web.UI.WebControls.Placeholder
    {
        protected override void Render(HtmlTextWriter writer)
        {
            var currentRendering = RenderingContext.CurrentOrNull?.Rendering;

            if (currentRendering == null)
            {
                base.Render(writer);
                return;
            }

            if (!IsMvcLayout(new ID(currentRendering.LayoutId)))
            {
                base.Render(writer);
                return;
            }

            RenderMvcPlaceholder(ContextKey, writer, currentRendering);
        }

        private void RenderMvcPlaceholder(string placeholder, TextWriter writer, Rendering ownerRendering)
        {
            const string pipeline = "mvc.renderPlaceholder";

            var args = new RenderPlaceholderArgs(Key,
                writer,
                ownerRendering);

            CorePipeline.Run(pipeline, args);
        }

        private bool IsMvcLayout(ID layoutId)
        {
            Item layoutItem = Sitecore.Context.Database.GetItem(layoutId);
            var layoutPath = layoutItem[Templates.Layout.Fields.Path];
            return !string.IsNullOrWhiteSpace(layoutPath) && layoutPath.EndsWith(".cshtml");
        }
    }
}