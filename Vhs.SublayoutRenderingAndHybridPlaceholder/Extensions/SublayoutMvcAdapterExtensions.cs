﻿using Sitecore.Collections;
using Sitecore.Mvc;
using Sitecore.Mvc.Presentation;
using Sitecore.Web;
using Sitecore.Web.UI.WebControls;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace Vhs.SublayoutRenderingAndHybridPlaceholder.Extensions
{
    public static class SublayoutMvcAdapterExtensions
    {
        public static IHtmlString RenderSitecoreSublayout(this HtmlHelper html, string path)
        {
            var rendering = html.Sitecore().CurrentRendering;
            return html.RenderSitecoreSubLayout(path, rendering.DataSource, rendering.Parameters);
        }

        public static IHtmlString RenderSitecoreSubLayout(this HtmlHelper html,
            string path, string datasource = null, RenderingParameters parameters = null)
        {
            var page = new Page();
            AddSublayoutToPage(page, path, datasource, parameters);
            var result = ExecutePage(page);

            return html.Raw(result);
        }

        private static void AddSublayoutToPage(Page page, string path, string datasource,
            IEnumerable<KeyValuePair<string, string>> parameters)
        {
            var dict = new SafeDictionary<string>();
            if (parameters != null)
            {
                parameters.ToList().ForEach(i => dict.Add(i.Key, i.Value));
            }

            var sublayout = new Sublayout
            {
                DataSource = datasource,
                Path = path,
                Parameters = WebUtil.BuildQueryString(dict, true, true)
            };

            var userControl = page.LoadControl(path) as UserControl;
            RecursiveLoadControl(userControl, page);
            sublayout.Controls.Add(userControl);
            page.Controls.Add(sublayout);
        }

        public static void RecursiveLoadControl(UserControl userControl, Page page)
        {
            foreach (var c in userControl.Controls)
            {
                if (c is Sublayout)
                {
                    var sl = (Sublayout)c;
                    var control = page.LoadControl(sl.Path) as UserControl;
                    control.Attributes.Add("sc_parameters", sl.Parameters);
                    control.Attributes.Add("sc_datasource", string.Empty);
                    RecursiveLoadControl(control, page);
                    sl.Controls.Add(control);
                }
            }
        }

        public static string RenderControl(Control control)
        {
            var sb = new StringBuilder();
            var sw = new StringWriter(sb);
            var writer = new HtmlTextWriter(sw);
            control.RenderControl(writer);
            return sb.ToString();
        }

        private static string ExecutePage(IHttpHandler page)
        {
            var text = new StringWriter();
            HttpContext.Current.Server.Execute(page, text, true);
            return text.ToString();
        }
    }
}
