﻿using Sitecore.Data;

namespace Vhs.SublayoutRenderingAndHybridPlaceholder
{
    public struct Templates
    {
        public struct Layout
        {
            public static readonly ID Id = new ID("{3A45A723-64EE-4919-9D41-02FD40FD1466}");

            public struct Fields
            {
                public static readonly ID Path = new ID("{A036B2BC-BA04-44F6-A75F-BAE6CD242ABF}");
            }
        }
    }
}