﻿using SC = Sitecore;

namespace Vhs.ImageCoordinatePickerField2.Services
{
    public static class ConfigurationService
    {
        public static string DialogWidth
            => SC.Configuration.Settings.GetSetting("Vhs.ImageCoordinatePickerField2.DialogWidth", "800");

        public static string DialogHeight
            => SC.Configuration.Settings.GetSetting("Vhs.ImageCoordinatePickerField2.DialogHeight", "600");

        public static string ImageFieldName
            =>
                SC.Configuration.Settings.GetSetting("Vhs.ImageCoordinatePickerField2.ImageFieldName",
                    "Hero Image");

        public static string ImageAlternateText
            =>
                SC.Configuration.Settings.GetSetting("Vhs.ImageCoordinatePickerField2.ImageAlternateText",
                    "There is NO image in the parent item.");
    }
}