﻿namespace Vhs.ImageCoordinatePickerField2.Constants
{
    public static class QueryStringKeys
    {
        public const string Value = "value";
        public const string ContainerId = "containerId";
        public const string Code = "code";
        public const string View = "view";
    }
}